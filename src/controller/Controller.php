<?php
include_once("model/Model.php");
include_once("model/Book.php");
include_once("view/BookListView.php");
include_once("view/BookView.php");
include_once("view/ErrorView.php");
include_once("model/DBModelTmpl.php");

/** The Controller is responsible for handling user requests, for exchanging data with the Model,
 * and for passing user response data to the various Views. 
 * @author Rune Hjelsvold
 * @see model/Model.php The Model class holding book data.
 * @see view/viewbook.php The View class displaying information about one book.
 * @see view/booklist.php The View class displaying information about all books.
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Controller {
	public $model;
	
	public static $OP_PARAM_NAME = 'op';
	public static $DEL_OP_NAME = 'del';
	public static $ADD_OP_NAME = 'add';
	public static $MOD_OP_NAME = 'mod';
	
	public function __construct()  
    {  
		session_start();
		try{
        	$this->model = new DBModel(); //Model(); //Should implement DBModel from DBModelTmpl.php
    	}catch(PDOException $e){
    		//If we for some reason cannot connect to the database we want the user to see an error page
    		$errorView = new ErrorView($e->getMessage());
    		/*
    		MUST use die because we need to end executing the rest of the script.
			If not using die we end up with a model that has a "db = null" and we can't really do stuff with a variable that is null/object that could not be created. WE DO NEED A PDO OBJECT!!!
			PDOException can't handle a null variable because it is actually not a PDO object and error is never thrown!!!
			This results in all functions failing in DBModelTmpl.php, if not checking for db connection before executing.
    		*/ 
			die($errorView->create()); //End executing the script!!!!!!
    	}
    } 
	
/** The one function running the controller code.
 */
	public function invoke()
	{
		if (isset($_GET['id']))
		{
			// show the requested book
			$book = $this->model->getBookById($_GET['id']);
			if ($book)
			{
				$view = new BookView($book, self::$OP_PARAM_NAME, self::$DEL_OP_NAME, self::$MOD_OP_NAME);
				$view->create();
			}
			else
			{
				$view = new ErrorView();
				$view->create();
			}
		}
		else 
		{

			/*
			Functions in DBModelTmpl.php throws a PDOException if something is wrong. This needs to be caught and handled appropriatly. This is done by sending the user to a error page with the error message from the PDOException. It might not be a good idea to give the user the raw error message from a PDOException, so in the model a custom error message is made and sent with the PDOException to the this controller!
			*/
			if (isset($_POST[self::$OP_PARAM_NAME]))//A book record is to be added, deleted, or modified
			{
				switch($_POST[self::$OP_PARAM_NAME]) 
				{
				case self::$ADD_OP_NAME : 
				    $book = new Book($_POST['title'], $_POST['author'], $_POST['description']);
				    
				    //if addBook fails we should display ErroView
				    try{
				    	$this->model->addBook($book);
				    }catch(PDOException $e){
				    	$errorView = new ErrorView($e->getMessage()); 
				    	$errorView->create();
				    }
				    break;
				case self::$DEL_OP_NAME : 
				try{
				    	$this->model->deleteBook($_POST['id']);
				    }catch(PDOException $e){
				    	$errorView = new ErrorView($e->getMessage()); 
				    	$errorView->create();
				    }
				    break;
				case self::$MOD_OP_NAME : 
				    $book = new Book($_POST['title'], $_POST['author'], $_POST['description'], $_POST['id']);
				    try{
				    	$this->model->modifyBook($book);
				    }catch(PDOException $e){
				    	$errorView = new ErrorView($e->getMessage()); 
				    	$errorView->create();
				    }
				    break;				
				}
			}

			// no special book is requested, we'll show a list of all available books

			//If the getBookList() fails(db might be down) we show the user an error page with an appropriate error message
			try{
				$books = $this->model->getBookList();
				$view = new BookListView($books, self::$OP_PARAM_NAME, self::$ADD_OP_NAME);
				$view->create();
			}catch(PDOException $e){
				$errorView = new ErrorView($e->getMessage()); 
				$errorView->create();
			}
		}
	}
}

?>