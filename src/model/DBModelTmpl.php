<?php
include_once("IModel.php");
include_once("Book.php");
include_once("constants.php");
/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($this->db) //$db
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            //Try connection. Catch if it fails
            try{
                $this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
                //Catching db connection avoids termination of the script and leaving a back trace to the user. Might releav username and password.
                //Give the user a normal error message, PDOException-message should be logged to a file or something for review. But we haven't implemented that yet.
                throw new PDOException("Database connection failed");
            }
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        
         try{
            //$result = array();
             $stmt = $this->db->query('SELECT * FROM book');

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $id = $row['id'];
                $title = $row['title'];
                $author = $row['author'];
                $description = $row['description'];

                //($title, $author, $description, $id = -1)
                $booklist[] = new Book($title, $author, $description, $id);
            }

            return $booklist;

            }catch(PDOException $e){
                //PDOException should be extracted and logged, but not given to the user!
                throw new PDOException("Could't get book list!");
            }
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        //Must use prepared statements
		$book = null;

        if(is_numeric($id) && $id > 0){

          try{

            $stmt = $this->db->prepare('SELECT * FROM book WHERE id = :id');
            $stmt->bindValue(':id', $id);
            $count = $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $id = $row['id'];
                $title = $row['title'];
                $author = $row['author'];
                $description = $row['description'];

                //($title, $author, $description, $id = -1)
                $book = new Book($title, $author, $description, $id);

               return $book;

              };
              
              return null;
             
          }catch(PDOException $e){
            //PDOException should be extracted and logged, but not given to the user!
            throw new PDOException("Could't get book by id");
          };

        }else{
            //For unvalid id's return null(Expected by the controller and the test!)
            return null;
        }

    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        //title, author should not be empty
        if(empty($book->title) || empty($book->author)){
            //check for empty title and author
            throw  new PDOException('Empty title or author');
        }else{

            try{
                $stmt = $this->db->prepare('INSERT INTO book (title, author, description)' . ' VALUES(:title, :author, :description)');
                $stmt->bindValue(':title', $book->title);
                $stmt->bindValue(':author', $book->author);
                $stmt->bindValue(':description', $book->description);

                $affectedNo = $stmt->execute(); //Returns true or false
                $lastId = $this->db->lastInsertId();
                $book->id = $lastId;
            }catch(PDOException $e){
                //PDOException should be extracted and logged, but not given to the user!
                throw new PDOException("Could not add ned book");
            }
        }
    }


    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        //title, author should not 
        if(empty($book->title) || empty($book->author) || $book->id == null){
            //check for empty title and author
            throw  new PDOException('Empty title, author or id = null');
        }else{

             try{
                $stmt = $this->db->prepare(
                    'UPDATE book'
                    . ' SET title = :title,'
                    . 'author = :author,'
                    . 'description = :description'
                    . ' WHERE id = :id');

                $stmt->bindValue(':title', $book->title);
                $stmt->bindValue(':author', $book->author);
                $stmt->bindValue(':description', $book->description);
                $stmt->bindValue(':id', $book->id);

                $success = $stmt->execute(); //Returns true or false
         }catch(PDOException $e){
                //PDOException should be extracted and logged, but not given to the user!
                throw new PDOException("Something went wrong when trying to modify the book!");
            }
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if(is_numeric($id) && $id > 0){
            try{
                $stmt = $this->db->prepare('DELETE FROM book WHERE id = :id');
                $stmt->bindValue(':id', $id);

                $result = $stmt->execute();
            }catch(PDOException $e){
                //PDOException should be extracted and logged, but not given to the user!
                throw new PDOException("Could not delete book!");
            }

        }else{
            throw new PDOException("Invalid id");
        }
    }
	
}

?>